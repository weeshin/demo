package com.example.demo;

//import net.anotheria.moskito.aop.annotation.Monitor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author nicklim
 */
//@Monitor
@RestController
public class TestController {
    @GetMapping("test1")
    public String test1() {
        return "hello , this is tutorial 1";
    }

    @GetMapping("test2")
    public Map<String, Object> test2() {
        Map<String, Object> m = new HashMap<>(10);
        m.put("username", "nicklim");
        m.put("token", "sowodfidfjdiiowjier");
        m.put("date", new Date());
        return m;
    }
}
