package com.example.demo;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Stream;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;

public class TestConsecutive {
    public static void main(String[] args) throws IOException {
        String fileName = "/home/nicklim/tmp/c.csv";

        List<String> cards = new ArrayList<>();
        //read file into stream, try-with-resources
        try (Stream<String> stream = Files.lines(Paths.get(fileName))) {
            cards = stream.collect(toList());
        } catch (IOException e) {
            e.printStackTrace();
        }


//        List<String> cards = Arrays.asList(
//                "4734520012345622", "4734520012345678", "4734520012345679", "4734520012345680",
//                "5491123456789123", "5491123456789124", "5491123456789126", "5771123456780011"
//        );

        Map<String, List> patterns = cardPattern(cards);

        List<String> results = new ArrayList<>();
        patterns.keySet().stream()
                .forEach(key -> {
                    List<String> patternCards = patterns.get(key);
                    System.out.println(key + ", total cards: " + patternCards.size());
//                    if(patternCards.size() > 1) {
                        List<String> sequenceCards = cal(patternCards, key);
                        sequenceCards.forEach(System.out::println);
                        results.addAll(sequenceCards);
//                    }
                });
        System.out.println("=================== Final Result ===================");
        results.forEach(System.out::println);
        System.out.println("Total: " +results.size());

    }

    public static Map cardPattern(List<String> cards) {

        Map m = cards.stream().collect(
                groupingBy(o -> o.substring(0, 12))
        );

        return m;
    }

    public static List<String> cal(List<String> patternCards, String pattern) {
        if(patternCards.size() < 2) {
            return new ArrayList<>(0);
        }

        List<Integer> allNumbers = patternCards.stream()
                .map(card -> Integer.parseInt(card.substring(card.length()-4, card.length())))
                .collect(toList());
        allNumbers.sort(Integer::compareTo);

        Set sequenceList = new HashSet();
        for(int i=0; i<allNumbers.size()-1; i++) {
            int a = allNumbers.get(i);
            int b = allNumbers.get(i+1);

            boolean finalResult = recursiveConsecutive(a, b, 3);
            if (finalResult) {
                sequenceList.add(a);
                sequenceList.add(b);
            }
        }

        return (List<String>) sequenceList.stream().sorted()
                .map( o -> pattern + o).collect(toList());
    }

    public static boolean recursiveConsecutive(int a, int b, int step) {
        boolean finalResult = false;
        List<Integer> numbers = Arrays.asList(a, b);
        for(int s=1; s < step; s++){
            boolean r = isConsecutive(numbers, s);
            if(r) {
                finalResult = r;
            }
        }
        return finalResult;
    }

    public static boolean isConsecutive(List<Integer> list, int step) {
        Iterator<Integer> it = list.iterator();
        if (!it.hasNext()) {
            return true;
        }

        int prev = it.next();
        while (it.hasNext()) {
            int curr = it.next();
            if (prev + step != curr || prev + step < prev) {
                return false;
            }
            prev = curr;
        }
        return true;
    }
}
