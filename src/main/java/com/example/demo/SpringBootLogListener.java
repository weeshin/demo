package com.example.demo;

import org.jpos.iso.ISOMsg;
import org.jpos.util.LogEvent;
import org.jpos.util.LogListener;
import org.slf4j.Logger;

public class SpringBootLogListener implements LogListener {
    private Logger log;

    public SpringBootLogListener(Logger log) {
        this.log = log;
    }

    @Override
    public LogEvent log(LogEvent ev) {
        StringBuilder sb = new StringBuilder();
        sb.append(ev.getTag()).append(": ")
                .append(ev.getRealm()).append(": ");

        synchronized (ev.getPayLoad()) {
            for (Object o : ev.getPayLoad()) {
                if (o instanceof ISOMsg) {

                    ISOMsg m = (ISOMsg) o;
                    for(int i =0; i<m.getMaxField(); i++) {
                        if(m.hasField(i)) {
                            sb.append("F").append(i).append("=").append(m.getString(i)).append(",");
                        }
                    }
                    log.info("Msg: {}", sb.toString());
                    return ev;
                }
            }
        }
        return ev;
    }
}
