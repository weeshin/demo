package com.example.demo;

import org.jpos.iso.*;
import org.jpos.iso.channel.XMLChannel;
import org.jpos.iso.packager.XMLPackager;
import org.jpos.util.LogSource;
import org.jpos.util.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * @author nicklim
 */
@Component
public class AppStartupRunner implements ApplicationRunner, ISORequestListener {
    private final RabbitTemplate rabbitTemplate;

    @Autowired
    public AppStartupRunner(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    @Override
    public void run(ApplicationArguments applicationArguments) throws Exception {

        Logger logger = new Logger();
        logger.addListener (new SpringBootLogListener(LoggerFactory.getLogger("jpos")));
        ServerChannel channel = new XMLChannel(new XMLPackager());
        ((LogSource)channel).setLogger (logger, "channel");
        ISOServer server = new ISOServer(8000, channel, null);
        server.setLogger (logger, "server");
        server.addISORequestListener (this);

        new Thread (server).start ();
    }

    @Override
    public boolean process(ISOSource source, ISOMsg m) {
        try {
            rabbitTemplate.convertAndSend(DemoApplication.topicExchangeName, "foo.bar.baz", "Receive ISO 8583!");
            m.setResponseMTI ();
            m.set (39, "00");
            source.send (m);
        } catch (ISOException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return true;
    }
}

